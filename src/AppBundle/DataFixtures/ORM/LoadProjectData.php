<?php

namespace AppBundle\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;

use AppBundle\Entity\Project;
class LoadProjectData extends AbstractFixture implements OrderedFixtureInterface
{

    function getOrder()
    {
        return 2;
    }

    public function load(ObjectManager $manager)
    {
        $internal = new Project();
        $internal->setName('INTERNAL');
        $internal->setCreatedAt(new \DateTime());
        $internal->setIsInternal(true);

        $manager->persist($internal);

        for ($i = 0; $i < 5; $i++) {
            $project = new Project();
            $project->setName('Project' . ($i+1));
            $project->setCreatedAt(new \DateTime());
            $project->setEndAt(new \DateTime('+6 months'));
            $project->setIsInternal(false);
            $project->setAM($this->getReference('am-1'));
            $this->setReference(sprintf('p-%s', $i), $project);

            $manager->persist($project);
        }

        $manager->flush();


    }
}