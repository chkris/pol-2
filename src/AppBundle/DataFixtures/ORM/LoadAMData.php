<?php

namespace AppBundle\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;

use AppBundle\Entity\AM;

class LoadAMData extends AbstractFixture implements OrderedFixtureInterface
{

    function getOrder()
    {
        return 1;
    }

    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 5; $i++) {
            $aM = new AM();
            $aM->setEmail(sprintf('cholewka.krzysztof%s@gmail.com', $i));
            $aM->setFirstName(sprintf('Kris%s', $i));
            $aM->setLastName(sprintf('Cholewka%s', $i));

            $manager->persist($aM);

            $this->setReference(sprintf('am-%s', $i), $aM);
        }

        $manager->flush();
    }
}