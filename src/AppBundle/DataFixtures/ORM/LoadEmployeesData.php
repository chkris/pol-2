<?php

namespace AppBundle\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;

use AppBundle\Entity\Employee;

class LoadEmployeeData extends AbstractFixture implements OrderedFixtureInterface
{

    function getOrder()
    {
        return 2;
    }

    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 5; $i++) {
            $employee = new Employee();
            $employee->setEmail(sprintf('cholewka.krzysztof%s@gmail.com', $i));
            $employee->setFirstName('Kris');
            $employee->setLastName('Cholewka');

            $manager->persist($employee);

            $am = $this->getReference('am-1');
            $project = $this->getReference('p-1');

            $employee->setAm($am);

            $manager->persist($employee);

        }

        $manager->flush();
    }
}