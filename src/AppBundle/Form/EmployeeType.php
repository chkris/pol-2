<?php
namespace AppBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EmployeeType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder
            ->add('firstName', 'text', [
                'required' => true,
                'label' => 'First name'
            ])

            ->add('lastName', 'text', [
                'required' => true,
                'label' => 'Last name'
            ])
            ->add('email', 'email', [
                'required' => false,
                'label' => 'Email'
            ])
            ->add('am', 'entity', [
                'class' => 'AppBundle:AM',
                'property' => 'firstName',
            ])
            ->add('submit', 'submit');
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\Employee'
        ]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'app_employee_type';
    }
}