<?php
namespace AppBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProjectType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder
            ->add('name', 'text', [
                'required' => true,
                'label' => 'Name'
            ])

            ->add('createdAt', 'datetime', [
                'required' => true,
                'label' => 'Created at'
            ])
            ->add('endAt', 'datetime', [
                'required' => false,
                'label' => 'End at'
            ])
            ->add('isInternal', 'choice', [
                'choices' => [0, 1]
            ])
            ->add('am', 'entity', [
                'class' => 'AppBundle:AM',
                'property' => 'firstName',
            ])
            ->add('submit', 'submit');
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\Project'
        ]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'app_project_type';
    }
}