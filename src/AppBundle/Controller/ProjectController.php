<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Project;
use AppBundle\Form\ProjectType;
use AppBundle\Services\AM\AMManagerException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class ProjectController extends Controller
{
    /**
     * @Route("app/project/create", name="app_project_create")
     * @Template
     */
    public function createAction(Request $request)
    {
        $project = new Project();

        $form = $this->createForm(new ProjectType(), $project);
        $form->handleRequest($request);

        if ($form->isValid()) {
            try {
                $this->get('app.project_manager')->save($project);
                $this->get('session')->getFlashBag()->add('success', 'Project created');
            } catch (AMManagerException $exception) {
                $this->get('logger')->error($exception->getMessage(), array('exception' => $exception));
                $this->get('session')->getFlashBag()->add('danger', 'Cannot create project');
            }
        }

        return array(
            'form' => $form->createView(),
        );
    }
}
