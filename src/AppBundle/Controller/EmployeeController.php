<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Employee;
use AppBundle\Form\EmployeeType;
use AppBundle\Services\AM\AMManagerException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class EmployeeController extends Controller
{
    /**
     * @Route("app/employee/create", name="app_employee_create")
     * @Template
     */
    public function createAction(Request $request)
    {
        $employee = new Employee();

        $form = $this->createForm(new EmployeeType(), $employee);
        $form->handleRequest($request);

        if ($form->isValid()) {
            try {
                $this->get('app.employee_manager')->save($employee);
                $this->get('session')->getFlashBag()->add('success', 'Employee created');
            } catch (AMManagerException $exception) {
                $this->get('logger')->error($exception->getMessage(), array('exception' => $exception));
                $this->get('session')->getFlashBag()->add('danger', 'Cannot create Employee');
            }
        }

        return array(
            'form' => $form->createView(),
        );
    }

    /**
     * @Route("app/employee/list", name="app_employee_list")
     * @Template
     */
    public function listAction()
    {
        return [
            'employees' => $this->getDoctrine()->getRepository('AppBundle:Employee')->findAll()
        ];
    }
}
