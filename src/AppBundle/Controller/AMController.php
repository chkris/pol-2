<?php

namespace AppBundle\Controller;

use AppBundle\Entity\AM;
use AppBundle\Form\AMType;
use AppBundle\Services\AM\AMManagerException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class AMController extends Controller
{
    /**
     * @Route("app/am/create", name="app_am_create")
     * @Template
     */
    public function createAction(Request $request)
    {
        $am = new AM();

        $form = $this->createForm(new AMType(), $am);
        $form->handleRequest($request);

        if ($form->isValid()) {
            try {
                $this->get('app.am_manager')->save($am);
                $this->get('session')->getFlashBag()->add('success', 'AM created');
            } catch (AMManagerException $exception) {
                $this->get('logger')->error($exception->getMessage(), array('exception' => $exception));
                $this->get('session')->getFlashBag()->add('danger', 'Cannot create product');
            }
        }

        return [
            'form' => $form->createView(),
        ];
    }

    public function listAction()
    {
        return [
            'ams' => $this->getDoctrine()->getRepository('AppBundle:AM')->findAll()
        ];
    }
}
