<?php

namespace AppBundle\Services\AM;

use AppBundle\Entity\AM;

interface AMManagerInterface
{
    public function save(AM $am);
}