<?php

namespace AppBundle\Services\AM;

use AppBundle\Entity\AM;
use Doctrine\ORM\EntityManager;

class AMManager
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function save(AM $am)
    {
        $this->entityManager->persist($am);

        try {
            $this->entityManager->flush();
        } catch (\Exception $exception) {
            throw new AMManagerException($exception);
        }
    }
}