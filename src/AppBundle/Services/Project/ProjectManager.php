<?php

namespace AppBundle\Services\Project;

use AppBundle\Entity\AM;
use AppBundle\Entity\Project;
use Doctrine\ORM\EntityManager;

class ProjectManager
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function save(Project $project)
    {
        $this->entityManager->persist($project);

        try {
            $this->entityManager->flush();
        } catch (\Exception $exception) {
            throw new ProjectManagerException($exception);
        }
    }
}