<?php

namespace AppBundle\Services\AM;

use AppBundle\Entity\Project;

interface ProjectManagerInterface
{
    public function save(Project $project);
}