<?php

namespace AppBundle\Services\Employee;

use AppBundle\Entity\Employee;

interface EmployeeManagerInterface
{
    public function save(Employee $employee);
}