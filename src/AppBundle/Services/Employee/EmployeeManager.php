<?php

namespace AppBundle\Services\Employee;

use AppBundle\Entity\AM;
use AppBundle\Entity\Employee;
use AppBundle\Entity\Project;
use Doctrine\ORM\EntityManager;

class EmployeeManager
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function save(Employee $employee)
    {
        $this->entityManager->persist($employee);

        try {
            $this->entityManager->flush();
        } catch (\Exception $exception) {
            throw new EmployeeManagerException($exception);
        }
    }
}