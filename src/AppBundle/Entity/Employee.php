<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EmployeeRepository")
 * @ORM\Table(name="employee")
 */
class Employee
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="first_name", type="string", nullable=false)
     * @Assert\NotNull()
     */
    private $firstName;

    /**
     * @ORM\Column(name="last_name", type="string", nullable=false)
     * @Assert\NotNull()
     */
    private $lastName;

    /**
     * @ORM\Column(name="emial", type="string", nullable=false)
     * @Assert\NotNull()
     */
    private $email;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="AM",
     *     inversedBy="employees"
     * )
     */
    private $am;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(
     *      targetEntity="Project",
     *      inversedBy="employees"
     * )
     */
    private $projects;

    public function __construct()
    {
        $this->projects = new ArrayCollection();
    }

    public function addProject(Project $project)
    {
        $this->projects[] = $project;
    }

    /**
     * @return mixed
     */
    public function getAm()
    {
        return $this->am;
    }

    /**
     * @param mixed $am
     */
    public function setAm(AM $am)
    {
        $this->am = $am;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }
}
