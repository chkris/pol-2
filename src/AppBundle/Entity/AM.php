<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AMRepository")
 * @ORM\Table(name="am")
 */
class AM
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="first_name", type="string", nullable=false)
     * @Assert\NotNull()
     */
    private $firstName;

    /**
     * @ORM\Column(name="last_name", type="string", nullable=false)
     * @Assert\NotNull()
     */
    private $lastName;

    /**
     * @ORM\Column(name="email", type="string", nullable=false)
     * @Assert\NotNull()
     */
    private $email;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Employee",
     *     mappedBy="am"
     * )
     * @ORM\JoinTable(name="am_employees")
     */
    private $employees;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Project",
     *     mappedBy="am"
     * )
     * @ORM\JoinTable(name="am_projects")
     */
    private $projects;

    /**
     * @return mixed
     */
    public function getEmployees()
    {
        return $this->employees;
    }

    /**
     * @return mixed
     */
    public function getProjects()
    {
        return $this->projects;
    }



    public function addProject(Project $project)
    {
        $this->projects[] = $project;
    }

    public function addEmployee(Employee $employee)
    {
        $this->employees[] = $employee;
    }

    public function setEmployees(Collection $employees)
    {
        $this->employees = $employees;
    }

    public function __construct()
    {
        $this->employees = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }
}
